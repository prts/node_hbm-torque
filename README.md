node_hbm-torque
=


Gateway Websocket server with a HBM digital transducer AED 9001 using serial port

Thanks to all folks writing and sharing similar code.

### Requirements
You need npm packages serialport and socket.io
```bash
$ npm install serialport
$ npm install socket.io
```

### Server
Start the server
```bash
$ node server.js
```

### Clients
Use a web navigator to open html files that connect to server.   
One client at a time. The WS server allows one connection/client only.

**torque.html** - A simple interface that continously reads values from AED.   
Configured to work with a 100Nm Screw Torque Transducer, type T4A, connected to AED.


**send_cmd.html** - A simple interface that let you send commands to the AED



