/*
 
*/

var serialport = require("serialport");
var SerialPort = serialport.SerialPort;

var io;
var aed;

//var options = {'tcpport':50000, 'targetport': 'com1', 'baud': 9600};
var options = {'tcpport':50000, 'targetport': '/dev/ttyS0', 'baud': 9600};

var __RTER = '\r\n';
var __STER = ';';
//var __RTER = '\r';
//var __STER = '\r';

var __RV = 'msv?2';
var __RMAX= 'msv?3';
var __CMAX = 'cpv';

/**
  Torque functions
*/
var TOR = function () {
  this._target = {};
  this.forever = false;
  this._actual = {};
  this._inst = {};
  
  this.check_cvmax = false;
  
  this._v = 0;
  this._vmax = 0;
}

TOR.prototype.open = function (port, baud) {
  var tor = this;
  tor._target = new SerialPort(port, {
                                      baudrate: baud,
                                      dataBis: 8,
                                      stopBits: 1,
                                      parity: 'even',//'none', 
                                      parser: serialport.parsers.readline(__RTER)
                                      });

  var inst = tor._inst;
  inst.v = {value:'_v',next:'vmax',cmd:__RMAX};
  inst.vmax = {value:'_vmax',next:'v',cmd:__RV};
  
  var target = tor._target;

  target.on('open', function() {
    console.log('Connected to target Serial Device ', port + ':' + baud);
    target.write(String.fromCharCode(1)); //Ctrl-A
    //target.write(String.fromCharCode(2));
  });

  target.on('data', function(data) {
    //console.log('Received from target: ' + data);
    if (tor.forever) {
      var actual = tor._actual;
      if (tor.check_cvmax) {
        target.write(__CMAX+__STER);
        tor.check_cvmax = false;
        tor._actual = inst.vmax;
      } else {
        target.write(actual.cmd+__STER);
        tor._actual = inst[actual.next];
      }
      tor[actual.value] = data;
    } else {
      var n = tor._currentData;
      if (n) n.next(null, data)
    }
  });

  target.on('disconnect', function(err) {
    if (err) console.log(err.message);
    console.log('Target disconnected.');
  });

  target.on('close', function() {
    console.log('Target closed.');
  });

  target.on('error', function(err) {
    console.log('Target error:', err.message);
    var n = tor._currentData;
    if (n) n.next(err, null)
  });
};


TOR.prototype.read = function (command, next) {
    var n = {};
    n.cmd = command;
    n.next = next;
    this._currentData = n;
    //console.log('Sent ...',command+__STER)
    this._target.write(command+__STER);
}


/** ****************************************************************************
*/


/**
  WS 'verb' functions
*/

var _tC = ['aid?', 'cof1', 'cdw', 'cdw?'];
var _C = [];
var _config = {};

var _read_config = function(socket) {
  var cmd = _C.shift();
  if (cmd)
    aed.read(cmd, function(err, data) {
                    if (err) {
                      console.log(err);
                      socket.emit('t_error', {'err': err.message});
                    } else {
                      _config[cmd] = data;
                      _read_config(socket);
                    }
                  })
  else socket.emit('config', {
                            'cmd': 'config',
                            'data': _config,
                            });
}

var _read = function(cmd, socket) {
  aed.read(cmd, function(err, msg) {
                  if (err) {
                    console.log(err);
                    socket.emit('data', {
                                        'cmd': cmd,
                                        'err': err,
                                        'data':msg,
                                        'flag':'error'
                                        });
                  } else {
                    socket.emit('data', {
                                        'cmd': cmd,
                                        'data': msg,
                                        });
                  }
                });
}

/** ****************************************************************************
*/



/**
  WebSocket server
 */
var run_wsd = function(tcpPort) {
  var _client = false;

  // run ws server
  io = require('socket.io')(tcpPort);
  /* Setup WebSocket event listener
   */
  io.on('connection', function(socket){
    if (_client) {
      console.log('I have already a client ...');
      socket.emit('busy')
      //socket.conn.close();
      return
    }
    _client = socket;
    _C = _tC.slice();
    _config = {};
    console.log('client connected');
    //aed._target.write(String.fromCharCode(1)); //Ctrl-A
    //aed._target.write(String.fromCharCode(2)); //Ctrl-B
    aed._target.write(String.fromCharCode(18)); //Ctrl-R
    
    socket.on('disconnect', function(){
      _client = false;
      aed.forever = false;
      //aed._target.write('stp'+__STER)
      console.log('client disconnected');
      //aed._target.write(String.fromCharCode(1));
      aed._target.write('DCL;');
    });

    socket.on('read', function(data){
      // check event validity
      if (!data || !data.cmd) return;
      _read(data.cmd, socket);
    });

    socket.on('readV', function(data){
      socket.emit('data', {
                          'cmd': 'msv',
                          'v': aed._v,
                          'vmax': aed._vmax
                          });
    });

    socket.on('clearV', function(data){
      console.log('clearV');
      aed.check_cvmax = true;
    });

    socket.on('forever', function(data){
      aed.forever = true;
      aed._actual = aed._inst.v;
      aed._target.write(__RV+__STER);
    });

    socket.on('readConfig', function(data){
      console.log('readConfig');
      _read_config(socket);
    });

  });
}

/** ****************************************************************************
*/


/**
  Start the AED-ws server
 */
var start = function(options) {
  /* set up some default options
  */
  var tcpPort = options.tcpport;
  var targetPort = options.targetport;
  var targetHost = options.targethost;
  var baud = options.baud;
  
  /* log server title
  */
  console.log('----------------------------------------------------');
  console.log("WebSocket to AED server");
  console.log('----------------------------------------------------');
  
  /* open a serial port and setup ws server
  */
  aed = new TOR();
  aed.open(targetPort, baud);


  console.log("Server is running, ws://127.0.0.1:" + tcpPort);
  run_wsd(tcpPort);

  console.log("----------------------------------------------------");
  console.log();
}

start(options);

